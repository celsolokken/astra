package astra.physics;

import java.util.ArrayList;
import java.util.List;

import astra.geom.Quad;
import astra.map.Map;
import astra.utils.QuadList;

public class PhysicsWorld {
	
	private Map map;
	
	private List<Body> bodies = new ArrayList<Body>();
	
	private QuadList quadList;
	
	public PhysicsWorld(Map map) {
		this.map = map;
		
		int w = map.getWidth() * map.getTileSize();
		int c = 1;
		
		this.quadList = new QuadList(w, c);
	}
	
	public void update(int delta) {
		for (int i = 0; i < bodies.size(); i++) {
			Body nextBody = bodies.get(i);
			float newX = nextBody.getX() + nextBody.getMotionX();
			float newY = nextBody.getY() + nextBody.getMotionY();
			boolean toLeft = nextBody.getMotionX() < 0;
			boolean toDown = nextBody.getMotionY() < 0;
			List<Quad> listL = quadList.retrieveList((int) newX);
			List<Quad> listR = quadList.retrieveList((int) (newX + nextBody.getWidth()));
		}
	}
	
}
