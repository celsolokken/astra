package astra.physics;

import astra.geom.Quad;

public abstract class Body implements Quad {
	
	public abstract float getMotionX();
	
	public abstract float getMotionY();
	
}
