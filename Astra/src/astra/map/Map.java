package astra.map;

public class Map {
	
	public int[][] tileDefs;
	public int[][][] tiles;
	
	private int width, height, tileSize;
	
	public Map(int width, int height, int tileSize) {
		this.width = width;
		this.height = height;
		this.tileSize = tileSize;
		tiles = new int[3][width][height];
	}
	
	public int getWidth() {
		return width;
	}
	
	public int getHeight() {
		return height;
	}
	
	public int getTileSize() {
		return tileSize;
	}
	
	/**
	 * uso: 
	 * int t = tiles[layer][x][y];
	 * int[] tileDef = tileDefs[t];
	 * 
	 * int sprite = tileDef[0]; etc...
	 * int type = tileDef[1];
	 * int y0 = tileDef[2];
	 * int y1 = tileDef[3];
	 */

}
