package astra.tools.mapbuilder;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JPanel;

public class GridPanel extends JPanel implements MouseListener {
	
	private int barX, barY;
	
	private final Color COLOR1 = new Color(0, 0, 0);
	private final Color COLOR2 = new Color(80, 80, 80);
	
	GridPanel() {
		addMouseListener(this);
	}
	
	public void paint(Graphics g) {
		super.paint(g);
		
		g.setColor(COLOR1);
		for (int i = 0; i < Map.width; i += 2)
			g.fillRect(barX + Constants.borderSize + 1 + i * 16 * Map.zoom, 0, 16 * Map.zoom, Constants.borderSize + 1);
		for (int j = 0; j < Map.height; j += 2)
			g.fillRect(0, barY + Constants.borderSize + 1 + j * 16 * Map.zoom, Constants.borderSize + 1, 16 * Map.zoom);
		
		g.setColor(COLOR2);
		for (int i = 1; i < Map.width; i += 2)
			g.fillRect(barX + Constants.borderSize + 1 + i * 16 * Map.zoom, 0, 16 * Map.zoom, Constants.borderSize + 1);
		for (int j = 1; j < Map.height; j += 2)
			g.fillRect(0, barY + Constants.borderSize + 1 + j * 16 * Map.zoom, Constants.borderSize + 1, 16 * Map.zoom);
		
		g.setColor(Color.WHITE);
		for (int i = 1; i <= Map.width; i++)
			g.drawString(i-1 + "", barX + i * 16 * Map.zoom - 6, 13);
		for (int j = 1; j <= Map.height; j++)
			g.drawString(j-1 + "", 6, barY + j * 16 * Map.zoom);
		
		g.setColor(Color.WHITE);
		g.fillRect(0, 0, Constants.borderSize + 1, Constants.borderSize + 1);
		
		g.setColor(Color.BLACK);
		g.fillPolygon(new int[] {0, Constants.borderSize, Constants.borderSize}, 
						new int[] {Constants.borderSize, Constants.borderSize, 0}, 3);
	}
	
	protected void update(int barX, int barY) {
		this.barX = barX;
		this.barY = barY;
		repaint();
	}

	public void mouseClicked(MouseEvent e) {}

	public void mousePressed(MouseEvent e) {
		int x = e.getX();
		int y = e.getY();
		
		if (x > Constants.borderSize || y > Constants.borderSize)
			return;
		
		double a = Math.atan2(x, y);
		System.out.println(a);
		
		Map.zoom++;
		repaint();
		MapBuilder.instance.mapPanel.setPreferredSize(new Dimension(Map.width * 16 * Map.zoom, Map.height * 16 * Map.zoom));
		//MapBuilder.instance.mapScrollPane.getHorizontalScrollBar().revalidate();
		//MapBuilder.instance.mapScrollPane.getHorizontalScrollBar().repaint();
	}

	public void mouseReleased(MouseEvent e) {}

	public void mouseEntered(MouseEvent e) {}

	public void mouseExited(MouseEvent e) {}

}
