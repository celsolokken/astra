package astra.tools.mapbuilder;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JPanel;
import javax.swing.JScrollPane;

public class MapPanel extends JPanel implements MouseListener {
	
	private static final long serialVersionUID = 1L;
	
	private final Color ORANGE = new Color(255, 155, 0);
	private final Color ORANGE2 = new Color(250, 190, 0);
	
	private GridPanel gridPanel;
	
	public MapPanel(GridPanel gridPanel) {
		this.gridPanel = gridPanel;
		addMouseListener(this);
		setPreferredSize(new Dimension(Map.width * 16 * Map.zoom, Map.height * 16 * Map.zoom));
	}
	
	public void paint(Graphics g) {
		
		long start = System.nanoTime();
		
		g.clearRect(0, 0, getWidth(), getHeight());
		
		//... draw tiles
		for (int layer = 0; layer < 3; layer++)
			for (int[] tile : Map.tiles[layer]) {
				g.setColor(Color.RED);
				g.fillRect(tile[0] * 16 * Map.zoom, tile[1] * 16 * Map.zoom, 16 * Map.zoom, 16 * Map.zoom);
			}
		
		g.setColor(Color.BLACK);
		for (int i = 1; i <= Map.width - 1; i++)
			g.drawLine(i * 16 * Map.zoom, Map.height * 16 * Map.zoom, i * 16 * Map.zoom, 0);
		for (int i = 1; i <= Map.height - 1; i++)
			g.drawLine(0, i * 16 * Map.zoom, Map.width * 16 * Map.zoom, i * 16 * Map.zoom);
		
		//gridPanel.update(getX(), getY());
		
		//System.out.println("Interval: " + (System.nanoTime() - start)/1000 + " nano/1000");
	}

	public void mouseClicked(MouseEvent e) {}

	public void mousePressed(MouseEvent e) {
		e.consume();
		//if (Map.selectedTile == -1)
			//return;
		
		int posX = e.getX() / (16 * Map.zoom);
		int posY = e.getY() / (16 * Map.zoom);
		
		for (int[] tile : Map.tiles[Map.selectedLayer])
			if (tile[0] == posX && tile[1] == posY)
				return;
		
		int[] tile = {posX, posY, 0};
		Map.tiles[Map.selectedLayer].add(tile);
		
		repaint(posX * 16 * Map.zoom, posY * 16 * Map.zoom, 16 * Map.zoom, 16 * Map.zoom);
	}

	public void mouseReleased(MouseEvent e) {}

	public void mouseEntered(MouseEvent e) {}

	public void mouseExited(MouseEvent e) {}
	
}
