package astra.tools.mapbuilder;

import java.awt.*;
import java.awt.event.*;
import java.awt.geom.Rectangle2D;

import javax.swing.*;

public class MapBuilder extends JFrame implements ComponentListener {
	
	public static MapBuilder instance;
	
	JPanel mapPanel, bottomPanel, rightPanel;
	GridPanel mapGridPanel;
	JScrollPane mapScrollPane;
	
	private JSplitPane split0;
	private JSplitPane split1;

	MapBuilder() {
		instance = this;
		
		setSize(800, 600);
		setMinimumSize(new Dimension(800, 600));
		
		addComponentListener(this);
		
		mapGridPanel = new GridPanel();
		
		mapPanel = new MapPanel(mapGridPanel);
		bottomPanel = new TestPanel(Color.green, "bottom");
		rightPanel = new TestPanel(Color.blue, "right");
		
		mapScrollPane = new JScrollPane(mapPanel);
		
		AdjustmentListener al = new AdjustmentListener() {
			public void adjustmentValueChanged(AdjustmentEvent e) {
				mapGridPanel.update(mapPanel.getX(), mapPanel.getY());
			}
		};
		mapScrollPane.getVerticalScrollBar().addAdjustmentListener(al);
		mapScrollPane.getHorizontalScrollBar().addAdjustmentListener(al);
		
		mapGridPanel.setLayout(null);
		mapGridPanel.add(mapScrollPane);
		
		split0 = new JSplitPane(JSplitPane.VERTICAL_SPLIT, mapGridPanel, bottomPanel);
		split1 = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, split0, rightPanel);
		
		add(split1);
		
		split0.setDividerLocation(getHeight() - 200);
		split1.setDividerLocation(getWidth() - 180);
		
		setLocationRelativeTo(null);
		setDefaultCloseOperation(EXIT_ON_CLOSE); //remove
		setVisible(true);
	}
	
	public static void main(String[] args) {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e) {
			e.printStackTrace();
		}
		new MapBuilder();
	}
	
	public void componentResized(ComponentEvent e) {
		split0.setDividerLocation(getHeight() - 200);
		split1.setDividerLocation(getWidth() - 180);
		mapScrollPane.setBounds(Constants.borderSize, Constants.borderSize, split1.getDividerLocation()
				- Constants.borderSize, split0.getDividerLocation() - Constants.borderSize);
		repaint();
	}

	public void componentMoved(ComponentEvent e) {
		
	}

	public void componentShown(ComponentEvent e) {
		
	}

	public void componentHidden(ComponentEvent e) {
		
	}
	
	class TestPanel extends JPanel {
		
		private Color color;
		private String name;
		
		public TestPanel(Color color, String name) {
			this.color = color;
			this.name = name;
		}
		
		public void paint(Graphics g) {
			super.paint(g);
			g.setColor(color);
			g.fillRect(0, 0, getWidth(), getHeight());
			g.setColor(new Color(255 - color.getRed(), 255 - color.getGreen(), 255 - color.getBlue()));
			Rectangle2D strBounds = g.getFontMetrics().getStringBounds(name, g);
			g.drawString(name, getWidth() / 2 - (int) strBounds.getWidth() / 2, getHeight() / 2 + (int) strBounds.getHeight() / 2);
		}
		
	}

}
