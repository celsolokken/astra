package astra.tools.mapbuilder;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Rectangle;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.plaf.basic.BasicScrollBarUI;

public class GridScrollBar  extends BasicScrollBarUI {
	
	final Color trackColor = new Color(184, 146, 122);
	final Color thumbColor = new Color(84, 46, 22);
	
    protected void paintTrack(Graphics g, JComponent c, Rectangle r) {
    	g.setColor(trackColor);
    	
    	int scroll = ((JScrollBar) c).getValue();
    	
    	for (int j = 1; j < Map.height; j += 2)
			g.fillRect(0, j * 16 * Map.zoom - scroll, 20, 16 * Map.zoom);
    }

    protected void paintThumb(Graphics g, JComponent c, Rectangle r) {
    	g.setColor(Color.BLACK);
    	g.fillRoundRect(r.x + 5, r.y + 5, r.width - 10, r.height - 10, 0, 0);
    }
    
    protected JButton createIncreaseButton(int orientation) {
    	return createDecreaseButton(orientation);
    }
    
    protected JButton createDecreaseButton(int orientation) {
        JButton jbutton = new JButton();
        jbutton.setPreferredSize(new Dimension(0, 0));
        jbutton.setMinimumSize(new Dimension(0, 0));
        jbutton.setMaximumSize(new Dimension(0, 0));
        return jbutton;
    }
    
}
