package astra.graphics.string;

public class StringPainter {
	
	public static int[] createIndices(String str) {
		int len = str.length();
		int[] result = new int[len];
		for (int i = 0; i < len; i++) {
			//properly assign indices
			result[i] = 42;
		}
		return result;
	}
	
	public static int draw(CharIndices indices, float posX, float posY) {
		int wid = 0;
		while (indices.hasNext()) {
			int next = indices.next();
			drawCharIndex(next, posX + wid, posY);
			wid += getCharWidth(next) + 1;
		}
		return wid;
	}
	
	public static int draw(int[] indices, float posX, float posY) {
		int wid = 0;
		for (int i = 0; i < indices.length; i++) {
			drawCharIndex(indices[i], posX + wid, posY);
			wid += getCharWidth(indices[i]) + 1;
		}
		return wid;
	}
	
	public static int getCharWidth(int charIndex) {
		//properly return, e.g charWidths[charIndex];
		return 13;
	}
	
	public static int getLineWidth(int[] indices) {
		int wid = 0;
		for (int i = 0; i < indices.length; i++)
			wid += getCharWidth(indices[i]) + 1;
		return wid;
	}
	
	public static int getLineWidth(CharIndices ci) {
		int wid = 0;
		while (ci.hasNext())
			wid += getCharWidth(ci.next()) + 1;
		return wid;
	}
	
	private static void drawCharIndex(int charIndex, float posX, float posY) {
		
	}

}
