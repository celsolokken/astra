package astra.graphics.string;

import java.util.ArrayList;
import java.util.List;

public class CharIndices {
	
	private int caret;
	private List<Integer> indices = new ArrayList<>();
	
	public CharIndices() {
		
	}
	
	public CharIndices(String str) {
		append(str, 0);
	}
	
	public void append(String str) {
		append(str, indices.size());
	}
	
	public void append(String str, int position) {
		int[] indices = StringPainter.createIndices(str);
		for (int i = 0; i < indices.length; i++)
			//this.indices.add(indices[i]);
			this.indices.add(position + i, (int) str.charAt(i));
	}
	
	public boolean hasNext() {
		if (caret == indices.size()) {
			caret = 0;
			return false;
		}
		return caret < indices.size();
	}
	
	public int next() {
		return indices.get(caret++);
	}
	
	public void print() {
		while (hasNext()) {
			System.out.print(next());
		}
	}
	
	public void println() {
		print();
		System.out.println();
	}
	
	public void printAsString() {
		while (hasNext()) {
			//properly print indices, e.g, System.out.println(alphabet[next()]);
			System.out.print((char) next());
		}
	}
	
	public void printlnAsString() {
		printAsString();
		System.out.println();
	}

}
