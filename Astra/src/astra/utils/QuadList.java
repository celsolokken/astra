package astra.utils;

import java.util.ArrayList;
import java.util.List;

import astra.geom.Quad;

public class QuadList {
	
	private List<Quad>[] quadLists;
	
	private int listWidth, listCount;
	
	public QuadList(int listWidth, int listCount) {
		this.listWidth = listWidth;
		this.listCount = listCount;
		quadLists = new List[listCount];
		for (int i = 0; i < listCount; i++) {
			quadLists[i] = new ArrayList<>();
		}
	}
	
	public void update() {
		for (int i = 0; i < listCount; i++) {
			List<Quad> list = quadLists[i];
			for (int j = 0; j < list.size(); j++) {
				Quad quad = list.get(j);
				
				if (quad.removeFlag()) {
					list.remove(j);
					continue;
				}
				
				int listLeft = (int) quad.getX() / listWidth;
				int listRight = ((int) quad.getX() + (int) quad.getWidth()) / listWidth;
				boolean notLeft = listLeft != i;
				boolean notRight = listRight != i;
				if (notLeft)
					quadLists[listLeft].add(quad);
				if (notRight)
					quadLists[listRight].add(quad);
				if (notLeft && notRight)
					list.remove(j);
			}
		}
	}
	
	public void add(Quad quad) {
		int listLeft = (int) quad.getX() / listWidth;
		int listRight = ((int) quad.getX() + (int) quad.getWidth()) / listWidth;
		quadLists[listLeft].add(quad);
		if (listRight != listLeft)
			quadLists[listRight].add(quad);
	}
	
	public List<Quad> retrieveList(int posX) {
		return quadLists[posX / listWidth];
	}

}
