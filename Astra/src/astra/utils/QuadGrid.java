package astra.utils;

import java.util.*;

import astra.geom.Quad;

@Deprecated
public class QuadGrid {
	
	private int gridWidth, gridHeight, gridCountX, gridCountY;
	
	private List<Quad>[][] grids;
	
	public QuadGrid(int gridWidth, int gridHeight, int gridCountX, int gridCountY) {
		this.gridWidth = gridWidth;
		this.gridHeight = gridHeight;
		this.gridCountX = gridCountX;
		this.gridCountY = gridCountY;
		grids = new ArrayList[gridCountX][gridCountY];
		
		for (int x = 0; x < gridCountX; x++)
			for (int y = 0; y < gridCountY; y++)
				grids[x][y] = new ArrayList<>();
	}
	
	public void update() {
		for (int x = 0; x < gridCountX; x++)
			for (int y = 0; y < gridCountY; y++) {
				List<Quad> grid = grids[x][y];
				for (int i = 0; i < grid.size(); i++) {
					checkQuad(grid.get(i), x, y, true);
				}
			}
	}
	
	public void add(Quad quad) {
		checkQuad(quad, quad.getX() / gridWidth, quad.getY() / gridHeight, false);
	}
	
	public List<Quad> getGridAt(int x, int y) {
		return grids[x / gridWidth][y / gridHeight];
	}
	
	private void checkQuad(Quad q, int gridX, int gridY, boolean checkCurrentGrid) {
		int qx = q.getX(), qy = q.getY(), qw = q.getWidth(), qh = q.getHeight();
		int gridX0 = gridX * gridWidth, gridX1 = gridX0 + gridWidth, gridY0 = gridY * gridHeight, gridY1 = gridY0 + gridHeight;
		
		if (!checkCurrentGrid)
			grids[gridX][gridY].add(q);
		
		if (!checkCurrentGrid || qx + qw >= gridX0 && qx < gridX1 && qy + qh >= gridY0 && qy < gridY1) {
			if (qx < gridX0)
				grids[gridX-1][gridY].add(q);
			else if (qx + qw > gridX1)
				grids[gridX+1][gridY].add(q);
			
			if (qy < gridY0)
				grids[gridX][gridY-1].add(q);
			else if (qy + qh > gridY0)
				grids[gridX][gridY+1].add(q);
		}
		else {
			grids[gridX][gridY].remove(q);
			checkQuad(q, q.getX() / gridWidth, q.getY() / gridHeight, false);
		}
	}
	
}
