package astra.utils;

import java.util.ArrayList;
import java.util.List;

import astra.geom.Quad;
import astra.geom.Rectangle;

public class QuadTree {
	
	private int MAX_OBJECTS = 10;
	private int MAX_LEVELS = 5;
	
	private int level;
	private List<Quad> objects;
	private Quad bounds;
	private QuadTree[] nodes;
	
	public QuadTree(int level, Quad bounds) {
		this.level = level;
		objects = new ArrayList<Quad>();
		this.bounds = bounds;
		nodes = new QuadTree[4];
	}
	
	public void clear() {
		objects.clear();
		
		for (int i = 0; i < nodes.length; i++) {
			if (nodes[i] != null) {
				nodes[i].clear();
				nodes[1] = null;
			}
		}
	}
	
	public void split() {
		int subWidth = (int) (bounds.getWidth() / 2);
		int subHeight = (int) (bounds.getHeight() / 2);
		int x = (int) bounds.getX();
		int y = (int) bounds.getY();
		
		nodes[0] = new QuadTree(level + 1, new Rectangle(x + subWidth, y, subWidth, subHeight));
		nodes[1] = new QuadTree(level + 1, new Rectangle(x, y, subWidth, subHeight));
		nodes[2] = new QuadTree(level + 1, new Rectangle(x, y + subHeight, subWidth, subHeight));
		nodes[3] = new QuadTree(level + 1, new Rectangle(x + subWidth, y + subHeight, subWidth, subHeight));
	}
	
	private int getIndex(Quad quad) {
		int index = -1;
		double verticalMidpoint = bounds.getX() + (bounds.getWidth() / 2);
		double horizontalMidpoint = bounds.getY() + (bounds.getHeight() / 2);
		
		boolean topQuadrant = quad.getY() < horizontalMidpoint && quad.getY() + quad.getHeight() < horizontalMidpoint;
		boolean bottomQuadrant = quad.getY() > horizontalMidpoint;
		
		if (quad.getX() < verticalMidpoint && quad.getX() + quad.getWidth() < verticalMidpoint) {
			if (topQuadrant)
				index = 1;
			else if (bottomQuadrant)
				index = 2;
		}
		else if (quad.getX() > verticalMidpoint) {
			if (topQuadrant)
				index = 0;
			else if (bottomQuadrant)
				index = 3;
		}
		return index;
	}
	
	public void insert(Quad quad) {
		if (nodes[0] != null) {
			int index = getIndex(quad);

			if (index != -1) {
				nodes[index].insert(quad);

				return;
			}
		}

		objects.add(quad);

		if (objects.size() > MAX_OBJECTS && level < MAX_LEVELS) {
			if (nodes[0] == null) {
				split();
			}

			int i = 0;
			while (i < objects.size()) {
				int index = getIndex(objects.get(i));
				if (index != -1) {
					nodes[index].insert(objects.remove(i));
				} else {
					i++;
				}
			}
		}
	}
	
	public List<Quad> retrieve(List<Quad> returnObjects, Quad quad) {
		int index = getIndex(quad);
		if (index != -1 && nodes[0] != null) {
			nodes[index].retrieve(returnObjects, quad);
		}

		returnObjects.addAll(objects);

		return returnObjects;
	}

}
