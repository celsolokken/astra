package astra.utils;

public class ObjectPool<O extends Poolable> {
	
	private Object[] pool;
	
	private int capacity;
	private int filled;
	private int next;
	
	public ObjectPool(int capacity) {
		pool = new Object[this.capacity = capacity];
	}
	
	public void add(O toAdd) throws IndexOutOfBoundsException {
		if (filled >= capacity)
			throw new IndexOutOfBoundsException("The object pool of capacity [" + capacity + "] is full.");
		pool[filled++] = toAdd;
	}
	
	@SuppressWarnings("unchecked")
	public O next() {
		return (O) pool[next++];
	}
	
	public boolean hasNext() {
		if (next < capacity)
			return true;
		else {
			next = 0;
			return false;
		}
	}
	
	static class Foo implements Poolable {
		
		int x;
		int life;
		int maxLife;
		int id;
		
		Foo(int id) {
			this.id = id;
			maxLife = id + 5;
		}

		public boolean isAlive() {
			return life < maxLife;
		}

		public void reset() {
			System.out.println("Removendo: " + id);
			x = 0;
			life = 0;
		}
	}

	/*public static void main(String... args) {
		ObjectPool<Foo> op = new ObjectPool<Foo>(5);
		op.add(new Foo(1));
		op.add(new Foo(2));
		op.add(new Foo(3));
		op.add(new Foo(4));
		op.add(new Foo(5));
		
		while (true) {
			
			while (op.hasNext()) {
				Foo f = op.next();
				if (!f.isAlive()) {
					f.reset();
					continue;
				}
				System.out.println(f.id + " : " +f.life++);
			}
			
			System.out.println();
			
			try {
				Thread.sleep(1000 * 4);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}*/
}
