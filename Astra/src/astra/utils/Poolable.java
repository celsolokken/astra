package astra.utils;

public interface Poolable {
	
	/**
	 * 
	 * @return false caso o objeto deva ser resetado
	 */
	boolean isAlive();

	/**
	 * M�todo chamado sempre que o objeto for resetado pela ObjectPool, que ocorre
	 * quando o m�todo deste objeto isAlive() retorna false.
	 */
	void reset();
}
