package astra.geom;

public class Rectangle implements Quad {
	
	private float x, y, width, height;
	
	public Rectangle(int x, int y, int width, int height) {
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
	}

	public float getX() {
		return x;
	}

	public float getY() {
		return y;
	}

	public float getWidth() {
		return width;
	}

	public float getHeight() {
		return height;
	}

	public void setX(int x) {
		this.x = x;
	}

	public void setY(int y) {
		this.y = y;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public boolean removeFlag() {
		return false;
	}

	public void setX(float x) {
		
	}

	public void setY(float y) {
		
	}

	public void setWidth(float width) {
		
	}

	public void setHeight(float height) {
		
	}

}
