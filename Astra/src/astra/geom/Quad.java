package astra.geom;

public interface Quad {
	
	float getX();
	
	float getY();
	
	float getWidth();
	
	float getHeight();
	
	void setX(float x);
	
	void setY(float y);
	
	void setWidth(float width);
	
	void setHeight(float height);
	
	boolean removeFlag();

}
